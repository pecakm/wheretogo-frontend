import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AddActivityComponent } from './Components/add-activity/add-activity.component';
import { PlaceDetailComponent } from './Components/place-detail/place-detail.component';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { AccountComponent } from './Components/account/account.component';
import { ProfileComponent } from './Components/profile/profile.component';
import { WelcomeComponent } from './Components/welcome/welcome.component';
import { SearchComponent } from './Components/search/search.component';

const routes: Routes = [
	{ path: '', component: SearchComponent },
	{ path: 'add', component: AddActivityComponent },
	{ path: 'place/:id', component: PlaceDetailComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'signup', component: SignupComponent },
	{ path: 'my-account', component: AccountComponent },
	{ path: 'user/:id', component: ProfileComponent },
	{ path: 'welcome', component: WelcomeComponent }
];

@NgModule({
	imports: [
		RouterModule.forRoot(routes)
	],
	exports: [
		RouterModule
	]
})
export class AppRoutingModule {}
