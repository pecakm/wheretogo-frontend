import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Services/user-service/user.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-signup',
	templateUrl: './signup.component.html',
	styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
	email = '';
	pass = '';
	disableButton = false;
	repeatPass: string;

	constructor(
		private userService: UserService,
		private router: Router
	) {}

	ngOnInit() {}

	signUp() {
		this.disableButton = true;
		if (!this.validateEmail(this.email)) {
			alert('Wpisz poprawny adres e-mail.');
			this.disableButton = false;
			return;
		}

		if (this.pass.length > 5 && this.pass.length < 33 && this.pass == this.repeatPass) {
			const credentials = {
				email: this.email,
				password: this.pass
			};
	
			this.userService.signUp(credentials)
			.subscribe(resp => {
				alert('Rejestracja powiodła się. Możesz teraz się zalogować.');
				this.router.navigate(['login']);
			}, error => {
				if (error.status == 422) {
					alert('Wybierz inny adres e-mail.');
				} else {
					console.log(alert);
				}
				
				this.disableButton = false;
			});
		} else {
			alert('Hasła muszą być równe i posiadać od 6 do 32 znaków.');
			this.disableButton = false;
		}
	}

	validateEmail(email: string) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	}
}
