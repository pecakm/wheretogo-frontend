import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Services/user-service/user.service';
import { User } from 'src/app/Models/User';

@Component({
	selector: 'app-welcome',
	templateUrl: './welcome.component.html',
	styleUrls: ['./welcome.component.scss']
})
export class WelcomeComponent implements OnInit {
	account: User;

	constructor(
		private userService: UserService
	) {}

	ngOnInit() {
		this.getAccountInfo();
	}

	getAccountInfo() {
		this.userService.getMyAccount()
		.subscribe(user => {
			this.account = user;

			this.userService.setActive(this.account.id)
			.subscribe(() => {}, error => {
				console.log(error);
			});
		});
	}
}
