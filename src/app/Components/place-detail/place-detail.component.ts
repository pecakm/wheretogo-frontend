import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Place } from '../../Models/Place';
import { PlaceService } from '../../Services/place-service/place.service';
import { CONSTANTS } from 'src/app/Helpers/Constants';

@Component({
	selector: 'app-place-detail',
	templateUrl: './place-detail.component.html',
	styleUrls: ['./place-detail.component.scss']
})
export class PlaceDetailComponent implements OnInit {
	place: Place;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private placeService: PlaceService,
	) {}

	ngOnInit() {
		this.getPlaceInfo();
	}

	getPlaceInfo() {
		const placeId = this.route.snapshot.paramMap.get('id');
		if (localStorage.getItem(CONSTANTS.TOKEN)) {
			this.placeService.getPlaceDetailLogged(placeId)
			.subscribe(place => {
				this.place = place;
			});
		} else {
			this.placeService.getPlaceDetail(placeId)
			.subscribe(place => {
				this.place = place;
			});
		}
	}

	addNewActivity() {
		this.placeService.place = this.place;
		this.router.navigate(['add']);
	}

	openMainMap() {
		this.router.navigate(['/']);
	}
}
