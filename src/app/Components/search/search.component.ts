import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Address } from 'ngx-google-places-autocomplete/objects/address';

import { CONSTANTS } from 'src/app/Helpers/Constants';
import { PlaceService } from '../../Services/place-service/place.service';
import { SearchService } from 'src/app/Services/search-service/search.service';
import { UserService } from 'src/app/Services/user-service/user.service';
import { User } from 'src/app/Models/User';
import { Place } from '../../Models/Place';
import { Location } from '../../Models/Location';

@Component({
	selector: 'app-search',
	templateUrl: './search.component.html',
	styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
	menuOpened = false;
	loading = true;
	cityName = 'Poznań';
	places: Place[] = [];
	currentPlace: Place;
	account: User;

	constructor(
		private router: Router,
		private placeService: PlaceService,
		private cookieService: CookieService,
		private searchService: SearchService,
		private userService: UserService
	) {}

	ngOnInit() {
		if (localStorage.getItem(CONSTANTS.TOKEN)) {
			this.loadAccountDetail();
		} else {
			this.loading = false;
		}

		this.selectCurrentPlace();
	}

	loadAccountDetail() {
		this.userService.getMyAccount()
			.subscribe(account => {
				account.imageUrl = CONSTANTS.BASE_API_URL + account.imageUrl;
				this.account = account;
				this.loading = false;
			});
	}

	selectCurrentPlace() {
		const placeId = this.cookieService.get(CONSTANTS.COOKIE_CURRENT_PLACE);

		if (placeId) {
			this.placeService.getPlaceDetail(placeId)
				.subscribe(place => {
					this.setCurrentPlace(place);
				});
		}
	}

	setCurrentPlace(place: Place) {
		this.currentPlace = place;
		this.cookieService.set(CONSTANTS.COOKIE_CURRENT_PLACE, place.id);
		this.loadPlaceLocations(place.location);
	}

	loadPlaceLocations(location: Location) {
		this.places = [];
		this.placeService.getPlaces(location)
			.subscribe(places => {
				this.places = places;
			});
	}

	toggleMenu() {
		this.menuOpened = !this.menuOpened;
	}

	closeMenu() {
		this.menuOpened = false;
	}

	logOut() {
		this.userService.logout();
		this.account = null;
	}

	changeCity() {
		alert('W tej chwili nie można wybrać innego miasta.');
	}

	chooseOption(address: Address) {
		this.searchService.searchGooglePlace(address)
			.subscribe(place => {
				this.setCurrentPlace(place);
			});
	}

	changeCurrentPlace() {
		this.currentPlace = null;
		this.places = [];
	}

	openPlaceDetail(placeId) {
		this.router.navigate(['place', placeId]);
	}
}
