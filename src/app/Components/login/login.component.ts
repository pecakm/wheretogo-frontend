import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/Services/user-service/user.service';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	disableButton = false;
	email: string;
	pass: string;

	constructor(
		private userService: UserService,
		private router: Router
	) {}

	ngOnInit() {}

	logIn() {
		this.disableButton = true;
		const credentials = {
			email: this.email,
			password: this.pass
		};

		this.userService.logIn(credentials)
		.subscribe(token => {
			if (token.token) {
				localStorage.setItem('token', token.token);

				if (token.active) {
					this.router.navigate(['/']);
				} else {
					this.router.navigate(['/welcome']);
				}
			}
		}, error => {
			alert('Niepoprawny login lub hasło.');
			this.disableButton = false;
		});
	}
}
