import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { CookieService } from 'ngx-cookie-service';
import { Ng2ImgMaxService } from 'ng2-img-max';

import { CONSTANTS } from 'src/app/Helpers/Constants';
import { ActivityService } from '../../Services/activity-service/activity.service';
import { PlaceService } from '../../Services/place-service/place.service';
import { SearchService } from 'src/app/Services/search-service/search.service';
import { Place } from 'src/app/Models/Place';
import { UserService } from 'src/app/Services/user-service/user.service';
import { User } from 'src/app/Models/User';

@Component({
	selector: 'app-add-activity',
	templateUrl: './add-activity.component.html',
	styleUrls: ['./add-activity.component.scss']
})
export class AddActivityComponent implements OnInit {
	disableButton = false;
	place: Place;
	author: User;
	activityText: string;
	selectedImage: File;
	imagePreview: string;

	constructor(
		private router: Router,
		private activityService: ActivityService,
		private placeService: PlaceService,
		private cookieService: CookieService,
		private searchService: SearchService,
		private userService: UserService,
		private ng2ImgMax: Ng2ImgMaxService,
		private sanitizer: DomSanitizer // need to add-activity.component.html
	) {}

	ngOnInit() {
		if (this.placeService.place) {
			this.place = this.placeService.place;
			this.placeService.place = null;
		}

		if (localStorage.getItem(CONSTANTS.TOKEN)) {
			this.userService.getMyAccount()
			.subscribe(account => {
				this.author = account;
			});
		}
	}

	chooseOption(address: Address) {
		this.searchService.searchGooglePlace(address)
		.subscribe(place => {
			this.setCurrentPlace(place);
		});
	}
	
	setCurrentPlace(place: Place) {
		this.place = place;
		this.cookieService.set(CONSTANTS.COOKIE_CURRENT_PLACE, place.id);
	}

	changePlace() {
		this.place = null;
	}

	fileSelected(event) {
		if (event.target.files[0] != null) {
			this.ng2ImgMax.compressImage(event.target.files[0], CONSTANTS.MAX_UPLOAD_SIZE)
			.subscribe(image => {
				this.selectedImage = image;
				this.getImagePreview(this.selectedImage);
			}, error => {
				console.log(error);
			});
		} else {
			this.selectedImage = null;
			this.imagePreview = null;
		}
	}

	getImagePreview(file: File) {
		const reader: FileReader = new FileReader();
		reader.readAsDataURL(file);
		reader.onload = () => {
		  	this.imagePreview = <string>reader.result;
		};
	}

	addActivity() {
		this.disableButton = true;
		let data = new FormData();
		data.append('placeId', this.place.id);
		
		if (this.author) {
			data.append('authorId', this.author.id);
		}

		if (this.activityText) {
			data.append('text', this.activityText);
		}

		if (this.selectedImage) {
			data.append('activityImage', this.selectedImage, this.selectedImage.name);
		}

		this.activityService.addActivity(data)
		.subscribe(activity => {
			this.showPlaceDetail(this.place.id);
		}, error => {
			console.log(error);
			this.disableButton = false;
		});
	}

	showPlaceDetail(placeId) {
		this.router.navigate(['place', placeId]);
	}

	openMainMap() {
		this.router.navigate(['/']);
	}
}
