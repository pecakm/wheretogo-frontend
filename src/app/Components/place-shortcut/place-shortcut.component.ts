import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Place } from 'src/app/Models/Place';

@Component({
	selector: 'app-place-shortcut',
	templateUrl: './place-shortcut.component.html',
	styleUrls: ['./place-shortcut.component.scss']
})
export class PlaceShortcutComponent implements OnInit {
	@Input() place: Place;

	constructor(
		private router: Router
	) {}

	ngOnInit() {}

	visited() {
		console.log('OK');
	}

	showDetail() {
		this.router.navigate(['place', this.place.id]);
	}
}
