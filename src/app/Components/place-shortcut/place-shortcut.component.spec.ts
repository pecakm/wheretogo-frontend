import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceShortcutComponent } from './place-shortcut.component';

describe('PlaceShortcutComponent', () => {
  let component: PlaceShortcutComponent;
  let fixture: ComponentFixture<PlaceShortcutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceShortcutComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceShortcutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
