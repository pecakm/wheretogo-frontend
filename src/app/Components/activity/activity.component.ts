import { Component, OnInit, Input } from '@angular/core';

import { CONSTANTS } from '../../Helpers/Constants';
import { LikeService } from 'src/app/Services/like-service/like.service';
import { Activity } from '../../Models/Activity';

@Component({
	selector: 'app-activity',
	templateUrl: './activity.component.html',
	styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
	@Input() activity: Activity;
	dateString: string;
	moreOpened = false;

	constructor(
		private likeService: LikeService
	) {}

	ngOnInit() {
		this.setDateString();
		this.setImage();
	}

	setDateString() {
		let currentDate = new Date(this.activity.date);
		let date = this.addZero(currentDate.getDate());
		let month = this.addZero(currentDate.getMonth() + 1);
		let year = currentDate.getFullYear();
		let hour = this.addZero(currentDate.getHours());
		let minutes = this.addZero(currentDate.getMinutes());
		this.dateString = `${date}-${month}-${year}, ${hour}:${minutes}`;
	}

	addZero(n: number): string | number {
		return n < 10 ? '0' + n : n;
	}

	setImage() {
		if (this.activity.imageUrl) {
			this.activity.imageUrl = CONSTANTS.BASE_API_URL + this.activity.imageUrl;
		}
	}

	toggleMore() {
		this.moreOpened = !this.moreOpened;
	}

	closeMore() {
		this.moreOpened = false;
	}

	report() {
		if (localStorage.getItem(CONSTANTS.TOKEN)) {
			alert('Post został zgłoszony');
		} else {
			alert('Aby zgłosić post musisz się zalogować.');
		}

		this.closeMore();
	}

	addLike() {
		if (localStorage.getItem(CONSTANTS.TOKEN)) {
			this.activity.userLiked = true;

			this.likeService.addLike(this.activity.id)
			.subscribe(likesCount => {
				this.activity.likesCount = likesCount;
			});
		} else {
			alert('Aby polubić, musisz się zalogować');
		}
	}

	openComments() {
		alert('Komentarze obecnie są niedostępne');
	}
}
