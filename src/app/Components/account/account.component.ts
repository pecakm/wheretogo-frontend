import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Services/user-service/user.service';
import { User } from 'src/app/Models/User';
import { CONSTANTS } from 'src/app/Helpers/Constants';
import { Router } from '@angular/router';

@Component({
	selector: 'app-account',
	templateUrl: './account.component.html',
	styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
	disableButton = false;
	account: User;
	usernameValue: string;
	fullnameValue: string;
	biogramValue: string;
	oldPassValue: string;
	newPassValue: string;
	repeatNewPassValue: string;

	constructor(
		private userService: UserService,
		private router: Router
	) {}

	ngOnInit() {
		this.getMyAccountDetail();
	}

	getMyAccountDetail() {
		this.userService.getMyAccount()
		.subscribe(account => {
			account.imageUrl = CONSTANTS.BASE_API_URL + account.imageUrl;
			this.account = account;
			this.usernameValue = account.username;
			this.fullnameValue = account.fullname;
			this.biogramValue = account.biogram;
		});
	}

	changeUsername() {
		if (this.usernameValue.length > 0) {
			this.userService.changeUsername(this.usernameValue)
			.subscribe(answer => {
				alert('Szczegóły konta zostały zaktualizowane.');
			}, error => {
				alert('Nie można wybrać tej nazwy użytkownika');
			});
		} else {
			alert('Wpisz nową nazwę użytkownika.');
		}
	}

	changeFullname() {
		this.userService.changeFullname(this.fullnameValue)
		.subscribe(answer => {
			alert('Szczegóły konta zostały zaktualizowane.');
		}, error => {
			console.log(error);
		});
	}

	changeBiogram() {
		this.userService.changeBiogram(this.biogramValue)
		.subscribe(answer => {
			alert('Szczegóły konta zostały zaktualizowane.');
		}, error => {
			console.log(error);
		});
	}

	changePassword() {
		this.disableButton = true;
		if (this.newPassValue.length > 5
		&& this.newPassValue.length < 33
		&& this.newPassValue == this.repeatNewPassValue) {
			this.userService.changePassword(this.oldPassValue, this.newPassValue)
			.subscribe(answer => {
				alert('Hasło zostało zaktualizowane. Musisz ponownie się zalogować.');
				this.router.navigate(['/']);
				this.userService.logout();
			}, error => {
				alert('Coś poszło nie tak. Spróbuj ponownie później.');
				this.disableButton = false;
			});
		} else {
			alert('Nowe hasła muszą być takie same i mieć od 6 do 32 znaków.');
			this.disableButton = false;
		}
	}
}
