import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

import { CONSTANTS } from 'src/app/Helpers/Constants';
import { UserService } from 'src/app/Services/user-service/user.service';
import { User } from 'src/app/Models/User';
import { Activity } from 'src/app/Models/Activity';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
	profile: User;

	constructor(
		private userService: UserService,
		private route: ActivatedRoute,
		private location: Location
	) {}

	ngOnInit() {
		if (localStorage.getItem(CONSTANTS.TOKEN)) {
			this.loadProfileInfo();
		} else {
			alert('Aby zobaczyć to konto musisz się zalogować');
		}
	}

	loadProfileInfo() {
		const id = this.route.snapshot.paramMap.get('id');

		this.userService.getUserDetail(id)
		.subscribe(user => {
			user.imageUrl = CONSTANTS.BASE_API_URL + user.imageUrl;
			this.profile = user;
		});
	}

	goBack() {
		this.location.back();
	}
}
