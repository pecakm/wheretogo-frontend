import { User } from "./User";

export class Activity {
    id: string;
    place: string;
    author: User;
    date: Date;
    text: string;
    imageUrl: string;
    likesCount: number;
    userLiked: boolean;
}