export class User {
    id: string;
    email: string;
    username: string;
    fullname: string;
    biogram: string;
    imageUrl: string;
}
