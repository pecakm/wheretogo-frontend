import { Location } from './Location';

export class Place {
    id: string;
    location: Location;
    address: string;
    distance: number;
    category: string;
    activitiesCount: number;
}
