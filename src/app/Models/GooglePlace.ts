import { Location } from './Location';

export class GooglePlace {
    location: Location;
    name: string;
    countryCode: string;
    postcode: string;
    state: string;
    locality: string;
    route: string;
    streetNumber: string;
    category: string;
}
