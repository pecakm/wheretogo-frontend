export class Token {
    token: string;
    active: boolean;
    exp: number;
}
