import { Directive, Input, ElementRef } from '@angular/core';

@Directive({
	selector: '[appAutofocus]'
})
export class AutofocusDirective {
	private element: any;

	constructor(private elementRef: ElementRef) {
		this.element = this.elementRef.nativeElement;
	}

	ngOnInit() {
		this.element.focus();
	}
}
