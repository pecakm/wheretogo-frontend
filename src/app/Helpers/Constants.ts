export const CONSTANTS = {
    // Map
    API_KEY: 'AIzaSyC15jEyX7HuiF8oazRf4U-54-OwCUDSfOc',

    // Urls
    BASE_API_URL: 'http://localhost:3000/',

    // Cookies
    COOKIE_CURRENT_PLACE: 'currentPlace',
    COOKIE_COUNTRY: 'country',

    // Local storage
    TOKEN: 'token',

    // Images
    FLAG_IMAGE_PATH: 'src/assets/world.png',
    MAX_UPLOAD_SIZE: 0.2 // in megabytes
}