import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CookieService } from 'ngx-cookie-service';
import { AgmCoreModule } from '@agm/core';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { ClickOutsideModule } from 'ng-click-outside';
import { Ng2ImgMaxModule } from 'ng2-img-max';

import { CONSTANTS } from './Helpers/Constants';
import { Interceptor } from './Helpers/Interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AddActivityComponent } from './Components/add-activity/add-activity.component';
import { PlaceDetailComponent } from './Components/place-detail/place-detail.component';
import { ActivityComponent } from './Components/activity/activity.component';
import { FooterComponent } from './Components/footer/footer.component';
import { LoginComponent } from './Components/login/login.component';
import { SignupComponent } from './Components/signup/signup.component';
import { ProfileComponent } from './Components/profile/profile.component';
import { AccountComponent } from './Components/account/account.component';
import { WelcomeComponent } from './Components/welcome/welcome.component';
import { NavbarComponent } from './Components/navbar/navbar.component';
import { AutofocusDirective } from './Directives/autofocus/autofocus.directive';
import { SearchComponent } from './Components/search/search.component';
import { PlaceShortcutComponent } from './Components/place-shortcut/place-shortcut.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent,
    AddActivityComponent,
    PlaceDetailComponent,
    ActivityComponent,
    AutofocusDirective,
    FooterComponent,
    LoginComponent,
    SignupComponent,
    ProfileComponent,
    AccountComponent,
    WelcomeComponent,
    NavbarComponent,
    SearchComponent,
    PlaceShortcutComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: CONSTANTS.API_KEY
    }),
    GooglePlaceModule,
    ClickOutsideModule,
    Ng2ImgMaxModule
  ],
  providers: [
    CookieService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {}
