import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { CONSTANTS } from '../../Helpers/Constants';
import { Token } from '../../Models/Token';
import { User } from '../../Models/User';

@Injectable({
	providedIn: 'root'
})
export class UserService {
	private logInUrl = CONSTANTS.BASE_API_URL + 'users/login';
	private signUpUrl = CONSTANTS.BASE_API_URL + 'users/signup';
	private getMyAccountUrl = CONSTANTS.BASE_API_URL + 'users/my-account';
	private getUserDetailUrl = CONSTANTS.BASE_API_URL + 'users/';
	private changeUsernameUrl = CONSTANTS.BASE_API_URL + 'users/my-account/username';
	private changeFullnameUrl = CONSTANTS.BASE_API_URL + 'users/my-account/fullname';
	private changeBiogramUrl = CONSTANTS.BASE_API_URL + 'users/my-account/biogram';
	private changePasswordUrl = CONSTANTS.BASE_API_URL + 'users/my-account/password';
	private setActiveUrl = CONSTANTS.BASE_API_URL + 'users/my-account/active/';

	constructor(private http: HttpClient) {}

	logIn(credentials): Observable<Token> {
		return this.http.post<Token>(this.logInUrl, credentials);
	}

	logout() {
		localStorage.removeItem(CONSTANTS.TOKEN);
	}

	signUp(credentials): Observable<string> {
		return this.http.post<string>(this.signUpUrl, credentials);
	}

	getMyAccount(): Observable<User> {
		return this.http.get<User>(this.getMyAccountUrl);
	}

	getUserDetail(id: string): Observable<User> {
		return this.http.get<User>(`${this.getUserDetailUrl}${id}`);
	}

	setActive(id: string): Observable<string> {
		return this.http.put<string>(`${this.setActiveUrl}${id}`, {});
	}

	changeUsername(username: string): Observable<string> {
		const queryBody = {
			username: username
		};

		return this.http.put<string>(this.changeUsernameUrl, queryBody);
	}

	changeFullname(fullname: string): Observable<string> {
		const queryBody = {
			fullname: fullname
		};

		return this.http.put<string>(this.changeFullnameUrl, queryBody);
	}

	changeBiogram(biogram: string): Observable<string> {
		const queryBody = {
			biogram: biogram
		};

		return this.http.put<string>(this.changeBiogramUrl, queryBody);
	}

	changePassword(oldPassword: string, newPassword: string) {
		const queryBody = {
			oldPassword: oldPassword,
			newPassword: newPassword
		};

		return this.http.put<string>(this.changePasswordUrl, queryBody);
	}
}
