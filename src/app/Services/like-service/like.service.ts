import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { CONSTANTS } from 'src/app/Helpers/Constants';

@Injectable({
	providedIn: 'root'
})
export class LikeService {
	private addLikeUrl = CONSTANTS.BASE_API_URL + 'activities/like/';

	constructor(
		private http: HttpClient
	) {}

	addLike(activityId: string): Observable<number> {
		return this.http.get<number>(`${this.addLikeUrl}${activityId}`);
	}
}
