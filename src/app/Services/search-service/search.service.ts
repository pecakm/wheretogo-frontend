import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { Observable } from 'rxjs';

import { CONSTANTS } from 'src/app/Helpers/Constants';
import { GooglePlace } from '../../Models/GooglePlace';
import { Place } from '../../Models/Place';

@Injectable({
	providedIn: 'root'
})
export class SearchService {
	private searchLocationsUrl = CONSTANTS.BASE_API_URL + 'places/search';

	options = {
		bounds: {
			south: 52.2903,
			west: 16.7292,
			north: 52.5104,
			east: 17.0758
		},
		strictBounds: true
	};

	constructor(
		private http: HttpClient
	) {}

	searchGooglePlace(address: Address): Observable<Place> {
		let place = new GooglePlace();
		place.location = {
			lng: address.geometry.location.lng(),
			lat: address.geometry.location.lat()
		};
		place.name = address.name;

		if (address.types.length > 0) {
			place.category = address.types[0];
		}

		for (let element of address.address_components) {
			if (element.types.length > 0) {
				if (element.types[0] == 'street_number') {
					place.streetNumber = element.short_name;
				}
	
				if (element.types[0] == 'route') {
					place.route = element.short_name;
				}

				if (element.types[0] == 'locality') {
					place.locality = element.short_name;
				}

				if (element.types[0] == 'administrative_area_level_1') {
					place.state = element.short_name;
				}

				if (element.types[0] == 'country') {
					place.countryCode = element.short_name;
				}

				if (element.types[0] == 'postal_code') {
					place.postcode = element.short_name;
				}
			}
		}

		return this.http.post<Place>(this.searchLocationsUrl, place);
	}
}
