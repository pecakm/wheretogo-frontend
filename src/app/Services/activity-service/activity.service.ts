import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { CONSTANTS } from '../../Helpers/Constants';
import { Activity } from '../../Models/Activity';

@Injectable({
	providedIn: 'root'
})
export class ActivityService {
	private getActivitiesUrl = CONSTANTS.BASE_API_URL + 'activities/';
	private addActivityUrl = this.getActivitiesUrl + '/add';

	constructor(private http: HttpClient) {}

	addActivity(activity: FormData): Observable<Activity> {
		return this.http.post<Activity>(this.addActivityUrl, activity);
	}
}
