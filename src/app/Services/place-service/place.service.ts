import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { CONSTANTS } from '../../Helpers/Constants';
import { Place } from '../../Models/Place';
import { Location } from '../../Models/Location';

@Injectable({
	providedIn: 'root'
})
export class PlaceService {
	private placeLocationsUrl = CONSTANTS.BASE_API_URL + 'places?';
	private placeDetailUrl = CONSTANTS.BASE_API_URL + 'places/detail/';
	private placeDetailLoggedUrl = CONSTANTS.BASE_API_URL + 'places/detail/user/'
	place: Place;

	constructor(
		private http: HttpClient
	) {}

	getPlaces(location: Location): Observable<Place[]> {
		return this.http.get<Place[]>(`${this.placeLocationsUrl}lng=${location.lng}&lat=${location.lat}`);
	}

	getPlaceDetail(id: string): Observable<Place> {
		const url = `${this.placeDetailUrl}${id}`;

		return this.http.get<Place>(url);
	}

	getPlaceDetailLogged(placeId: string): Observable<Place> {
		const url = `${this.placeDetailLoggedUrl}${placeId}`;

		return this.http.get<Place>(url);
	}
}
